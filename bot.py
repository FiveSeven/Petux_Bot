import discord
from discord.ext import commands
from discord import Game, Embed
from asyncio import sleep
import random
import youtube_dl
import re
from discord import opus
import os
import sys

me = commands.Bot(command_prefix='!', self_bot=True)
pref = '!'
allow_to_set = ['309002050958655488','277734416728588289']
fiveseven = ['309002050958655488']

def move_balance(usr_id1,usr_id2,amount):
	if amount <= 0:
		return(False)
	if check_balance(usr_id1) < amount:
		return(False)
	remove_balance(usr_id1,amount)
	give_balance(usr_id2,amount)
	return(True)


def remove_balance(usr_id,amount):
	if amount <= 0:
		return(False)
	balance = check_balance(usr_id) - amount
	if balance < 0:
		set_balance(usr_id,0)
		return()
	set_balance(usr_id,balance)

def give_balance(usr_id,amount):
	if amount <= 0:
		return(False)
	balance = amount + check_balance(usr_id)
	set_balance(usr_id,balance)


def set_balance(usr_id,amount):
	f = open('coins.txt','r')
	users = f.read().splitlines()
	f.close()
	ch_amount = int(amount)
	f = open('coins.txt','w')
	ch = False
	for i in users:
		if int(i.split(';')[0]) != usr_id:
			f.write(i+'\n')
		else:
			f.write(i.split(';')[0]+';'+str(ch_amount)+'\n')
			ch = True
	if ch == False:
		f.write(str(usr_id)+';'+str(ch_amount)+'\n')
	f.close()

def check_balance(usr_id):
	f = open('coins.txt','r')
	users = f.read().splitlines()
	f.close()
	balance = None
	for i in users:
		user = int(i.split(';')[0])
		if user == usr_id:
			balance = int(i.split(';')[1])
			return(balance)
	return(0)

def answerQuery():
    responses = ["Не уверен", "42", "Скорее всего", "Точно нет", "Выглядит хорошо", "Никогда", "Нет", "Может быть", "Не уверен, спроси снова", "Да", "Нет", "Возможно, но мало вероятно"]
    return(random.choice(responses))

def to_leet(string):
	for char in string:
		if char == 'a':
			string = string.replace('a','4')
		elif char == 'b':
			string = string.replace('b','8')
		elif char == 'e':
			string = string.replace('e','3')
		elif char == 'l':
			string = string.replace('l','1')
		elif char == 'o':
			string = string.replace('o','0')
		elif char == 's': 
			string = string.replace('s','5')
		elif char == 't':
			string = string.replace('t','7')
		else:
			pass
	
	return(string)


@me.event
async def on_ready():
    print('Logged in!\nName: {}\nId: {}'.format(
        me.user.name, me.user.id))
    if sys.maxsize > 2**32:
        opus.load_opus('libopus-0.x64.dll')
    else:
        opus.load_opus('libopus-0.x86.dll')
    await me.change_presence(game=Game(name="Dota 2"))


def make_embed(desc,ico):
    embed = discord.Embed(description=desc,colour=discord.Color.blue())
    embed.set_image(url=ico)
    return embed

def id_get(message,data):
	if str(data).isdigit():
		return(data)
	usr_id = re.findall('(\d+)', message)[0]
	return(int(usr_id))

@me.event
async def on_message(message):
    if message.author.id == me.user.id:
        return

    if message.content.startswith(pref+'help'):
        f = open("help.txt",'r', encoding='utf8')
        lines = f.read()
        f.close()
        await me.send_message(message.channel, lines)

    if message.content.startswith(pref+'choose'):
        choose = message.content[7:].split(";")
        choose = str(random.choice(choose))
        await me.send_message(message.channel, choose)
    
    if message.content.startswith(pref+'leet'):
        await me.send_message(message.channel, to_leet(message.content[5:]))

    if message.content.startswith(pref+'ques'):
        await me.send_message(message.channel, answerQuery())

    if message.content.startswith(pref+'money'):
        await me.send_message(message.channel, embed=make_embed('У вас '+str(check_balance(int(message.author.id)))+' семёркойнов','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))

    if message.content.startswith(pref+'add'):
        if message.author.id not in allow_to_set:
            await me.send_message(message.channel, embed=make_embed('У вас нет на это прав!','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))
            return()
        give_balance(int(id_get(message.content,message.content.split(' ')[1])),int(message.content.split(' ')[2]))
        await me.send_message(message.channel, embed=make_embed('Вы выдали '+str(message.content.split(' ')[1])+' '+str(message.content.split(' ')[2])+' семёркойнов','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))

    if message.content.startswith(pref+'remove'):
        if message.author.id not in allow_to_set:
            await me.send_message(message.channel, embed=make_embed('У вас нет на это прав!','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))
            return()
        remove_balance(int(id_get(message.content,message.content.split(' ')[1])),int(message.content.split(' ')[2]))
        await me.send_message(message.channel, embed=make_embed('Вы забрали у '+str(message.content.split(' ')[1])+' '+str(message.content.split(' ')[2])+' семёркойнов','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))

    if message.content.startswith(pref+'give'):
        if move_balance(int(message.author.id),int(id_get(message.content,message.content.split(' ')[1])),int(message.content.split(' ')[2])) != True:
            await me.send_message(message.channel, embed=make_embed('У вас нет столько!','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))
            return()
        await me.send_message(message.channel, embed=make_embed('Вы передали '+str(message.content.split(' ')[1])+' '+str(message.content.split(' ')[2])+' семёркойнов','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))

    if message.content.startswith(pref+'warn'):
        if message.author.id not in fiveseven:
            await me.send_message(message.channel, embed=make_embed('У вас нет на это прав!','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))
            return()
        await me.send_message(message.channel, 'You issued a warning to '+str(message.content.split(' ')[1]))

#    if message.content.startswith(pref+'join'):
#        channel = message.author.voice.voice_channel
#        await me.join_voice_channel(channel)

#    if message.content.startswith(pref+'leave'):
#        server = message.server
#        voice_client = me.voice_client_in(server)
#        await voice_client.disconnect()


    if message.content.startswith(pref+'play'):

        url = message.content[len(pref+'play '):]

        if message.author.voice_channel:

            try:
                if check_balance(int(message.author.id)) < 5:
                    await me.send_message(message.channel, embed=make_embed('У вас не хватает денег!','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))
                    return()

                channel = message.author.voice.voice_channel
                await me.join_voice_channel(channel)
    
                server = message.server
                voice_client = me.voice_client_in(server)
                player = await voice_client.create_ytdl_player(url)
                player.volume = 0.2
                player.start()
    
                if move_balance(int(message.author.id),0,5) != True:
                    await me.send_message(message.channel, embed=make_embed('У вас не хватает денег!','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))
                    return()
                await me.send_message(message.channel, embed=make_embed('Вы приобрели %music_player% за 5 семёркойнов','https://cdn.discordapp.com/attachments/462901153986772993/463464695320281089/1.png'))

            except:
            	print('vsy slomalos')
            while True:
                await sleep(3)
                try:
                    if player.is_done():
                        server = message.server
                        voice_client = me.voice_client_in(server)
                        await voice_client.disconnect()
                        break
                except:
                    server = message.server
                    voice_client = me.voice_client_in(server)
                    await voice_client.disconnect()
                    break
        else:
            await me.send_message(message.channel, 'Вы не находитесь в каком-то канале!')

token = ""  # Replace this with your user token
me.run(token, bot=False)

