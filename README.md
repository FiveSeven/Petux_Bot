# Petux Bot v1.0
Petux bot is a simple bot for SkyLine discord.

The latest release can be found here: [Release link](https://gitlab.com/FiveSeven/Petux_Bot/-/archive/master/Petux_Bot-master.zip)

## Discord
You can join our Discord here: [Discord link](https://discord.gg/bS5PrM4)

## SkyLine Bot Installation:
To install:
1. Download and extract bot.py.
2. Paste your user token to bot.py
3. Start bot.py
4. enjoy

Known bugs:
- no bugs c:

Place any suggestions/problems in issues!

Thanks & Enjoy.
